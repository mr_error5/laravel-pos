<?php

namespace App\Http\Controllers\Dashboard;
use Illuminate\Validation\Rule;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

        $categories = Category::when($request->search, function ($q) use ($request) {

            return $q->whereTranslationLike('name', '%' . $request->search . '%');

        })->latest()->paginate(5);

        return view('Dashboard.categories.index', compact('categories'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

          return view('dashboard.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       $data = $request->validate([

            'ar.*' => 'required|unique:category_translations,name',

        ]);
    //    $data = [

    //     app()->getLocale() => [

    //         'name' => $request->name
    //     ]

    //     ];

        // return dd($data);




       Category::create($request->all());
        session()->flash('success',__('site.add_success'));
        return redirect()->route('categories.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //

        return view('dashboard.categories.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //

        // $data = $request->validate([
        //     'name' => 'required|unique:categories,name,' . $category->id,

        // ]);
        $data = $request->validate([

            'ar.name' => ['required',Rule::unique('category_translations','name')->ignore($category->id,'category_id'),],
            'en.name' => 'string',

        ]);
        $category->update($data);

         session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('categories.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //

                $category->delete();
                session()->flash('success', __('site.deleted_successfully'));
                return redirect()->route('categories.index');
    }
}
