<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['prefix' => LaravelLocalization::setLocale(),'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]], function(){


Route::prefix('dashboard')->middleware(['auth'])->group(function(){

		/** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/
		Route::get('index','Dashboard\DashboardController@index')->name('dashboard.index');

		Route::resource('users','Dashboard\UserController');
		Route::resource('categories','Dashboard\CategoryController');
		Route::resource('products','Dashboard\ProductController');

	});


});

		Route::get('/', function () {
    return redirect()->route('dashboard.index');
});

Auth::routes(['register' => false]);

//Route::get('/home', 'HomeController@index')->name('home');
