@extends('layouts.dashboard.app')

@section('content')

         <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            {{ __('site.categories') }}
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ __('site.dashboard') }}</a></li>
            <li ><a href="{{ route('categories.index') }}">{{ __('site.categories') }}</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Data Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <form action="{{ route('categories.index') }}" method="get">
                <div class="row">

                  <div class="col-md-4">
                    <input type="text" name="search" class="form-control">
                  </div>
                  <div class="col-md-4">
                   <button type="submit" class="btn btn-primary"><i class="fa fa-search"> </i> {{ __('site.search') }}</button>
                   @if(auth()->user()->hasPermission('create_categories'))
                   <a href="{{ route('categories.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> {{ __('site.add') }}</a>
                   @else
<a href="{{ route('categories.create') }}" class="btn btn-primary disabled"><i class="fa fa-plus"></i> {{ __('site.add') }}</a>


                   @endif
                  </div>


                </div>

              </form>
              <table id="example1" class="table table-hover table-striped">
                <thead>
                <tr>
                  <th>{{ __('site.name') }}</th>
                  <th>{{ __('site.product_count') }}</th>
                  <th>{{ __('site.related_products') }}</th>

                  <th>{{ __('site.action') }}</th>
                  {{-- <th>Engine version</th> --}}
                  {{-- <th>CSS grade</th> --}}
                </tr>
                </thead>
                <tbody>

                  @foreach($categories as $category)
                <tr>
                  <td>{{ $category->name }}</td>
                  <td>{{ $category->products->count() }}</td>
                  <td><a href="{{ route('products.index', ['category_id' => $category->id]) }}" class="btn btn-info btn-sm">@lang('site.related_products')</a></td>



                  <td>
                    @if(auth()->user()->hasPermission('update_categories'))
                    <a class="btn btn-info" href="{{ route('categories.edit',$category->id) }}">{{ __('site.edit') }}</a>
                    {{-- <a href="{{ route('categories.destroy',$category->id) }}"></a> --}}
                    @else

                    <a class="btn btn-info disabled" href="#">{{ __('site.edit') }}</a>

                    @endif
                    @if(auth()->user()->hasPermission('delete_categories'))

                    <form style="display: inline-block;" action="{{ route('categories.destroy',$category->id) }}" method="post">
                      {{ csrf_field() }}
                      {{ method_field('delete') }}
                      <button type="submit" class="btn btn-danger delete">{{ __('site.delete') }}</button>

                    </form>
                    @else
                    <button type="submit" class="btn btn-danger disabled">{{ __('site.delete') }}</button>

                    @endif
                  </td>

                </tr>
                  @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>{{ __('site.name') }}</th>
                  <th>{{ __('site.product_count') }}</th>
                  <th>{{ __('site.related_products') }}</th>



                  <th>{{ __('site.action') }}</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          </div><!-- /.row -->
          <!-- Main row -->


        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->



@endsection

