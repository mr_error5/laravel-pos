@extends('layouts.dashboard.app')

@section('content')

         <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            {{ __('site.add') }}
          
          </h1>
          <ol class="breadcrumb">
          <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ __('site.dashboard') }}</a></li>
            <li ><a href="{{ route('categories.index') }}">{{ __('site.categories') }}</a></li>
            <li >{{ __('site.add') }}</li>

          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Data Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

 @include('partials._errors')

                    <form action="{{ route('categories.store') }}" method="post">

                        {{ csrf_field() }}
                        {{ method_field('post') }}

                      

                        @foreach (config('translatable.locales') as $locale)
                        <div class="form-group">
                          <label>@lang('site.' .$locale . '.name')</label>
                          <input type="text" name="{{ $locale }}[name]" class="form-control" value="{{ old($locale . '.name') }}">
                      </div>
                        @endforeach

               

                        
                     


                        <div class="form-group">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> @lang('site.add')</button>
                        </div>


                

                    </form><!-- end of form -->

       
            </div>
            <!-- /.box-body -->
          </div>
          </div><!-- /.row -->
          <!-- Main row -->


        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->



@endsection

