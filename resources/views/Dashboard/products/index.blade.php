@extends('layouts.dashboard.app')

@section('content')

         <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            {{ __('site.products') }}
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ __('site.dashboard') }}</a></li>
            <li ><a href="{{ route('products.index') }}">{{ __('site.products') }}</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Data Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <form action="{{ route('products.index') }}" method="get">
                <div class="row">

                </div>
                <div class="col-md-4">
                  <select name="category_id" class="form-control">
                      <option value="">@lang('site.all_categories')</option>
                      @foreach ($categories as $category)
                          <option value="{{ $category->id }}" {{ request()->category_id == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                      @endforeach
                  </select>
              </div>

                  <div class="col-md-4">
                    <input type="text" name="search" class="form-control">
                  </div>
                  <div class="col-md-4">
                   <button type="submit" class="btn btn-primary"><i class="fa fa-search"> </i> {{ __('site.search') }}</button>
                   @if(auth()->user()->hasPermission('create_products'))
                   <a href="{{ route('products.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> {{ __('site.add') }}</a>
                   @else
<a href="{{ route('products.create') }}" class="btn btn-primary disabled"><i class="fa fa-plus"></i> {{ __('site.add') }}</a>


                   @endif




                </div>

              </form>
              <table id="example1" class="table table-hover table-striped">
                <thead>
                <tr>
                  <th>{{ __('site.name') }}</th>
                  <th>{{ __('site.description') }}</th>
                  {{-- <th><img src="" alt=""></th> --}}
                  <th>{{ __('site.image') }}</th>
                  <th>{{ __('site.purchase_price') }}</th>
                  <th>{{ __('site.sale_price') }}</th>
                  <th>{{ __('site.profit_percent') }}</th>

                  <th>{{ __('site.stock') }}</th>


                  <th>{{ __('site.action') }}</th>
                  {{-- <th>Engine version</th> --}}
                  {{-- <th>CSS grade</th> --}}
                </tr>
                </thead>
                <tbody>

                  @foreach($products as $product)
                <tr>
                  <td>{{ $product->name }}</td>
                  <td>{!! $product->description !!}</td>
                  <th><img src="{{ $product->image_path }}" class="img-thumbnail" style="width:60px" alt=""></th>

                  <td>{{ $product->purchase_price }}</td>
                  <td>{{ $product->sale_price }}</td>
                  <td>{{ $product->profit_percent }} %</td>
                  <td>{{ $product->stocks }}</td>


                  <td>
                    @if(auth()->user()->hasPermission('update_products'))
                    <a class="btn btn-info" href="{{ route('products.edit',$product->id) }}">{{ __('site.edit') }}</a>
                    {{-- <a href="{{ route('products.destroy',$product->id) }}"></a> --}}
                    @else

                    <a class="btn btn-info disabled" href="#">{{ __('site.edit') }}</a>

                    @endif
                    @if(auth()->user()->hasPermission('delete_products'))

                    <form style="display: inline-block;" action="{{ route('products.destroy',$product->id) }}" method="post">
                      {{ csrf_field() }}
                      {{ method_field('delete') }}
                      <button type="submit" class="btn btn-danger delete">{{ __('site.delete') }}</button>

                    </form>
                    @else
                    <button type="submit" class="btn btn-danger disabled">{{ __('site.delete') }}</button>

                    @endif
                  </td>

                </tr>
                  @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>{{ __('site.name') }}</th>
                  <th>{{ __('site.description') }}</th>
                  {{-- <th><img src="" alt=""></th> --}}
                  <th>{{ __('site.image') }}</th>

                  <th>{{ __('site.purchase_price') }}</th>
                  <th>{{ __('site.sale_price') }}</th>
                  <th>{{ __('site.profit_percent') }}</th>
                  <th>{{ __('site.stock') }}</th>
                  <th>{{ __('site.action') }}</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          </div><!-- /.row -->
          <!-- Main row -->


        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->



@endsection

