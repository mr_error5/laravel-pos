  <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="{{ asset('rtl/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>{{ auth()->user()->name }}</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
              <a href="{{ route('dashboard.index') }}">
                <i class="fa fa-dashboard"></i> <span>{{ __('site.dashboard') }}</span>
              </a>

            </li>
              @if(auth()->user()->hasPermission('read_users'))

            <li class="treeview">
              <a href="{{ route('users.index') }}">
                <i class="fa fa-dashboard"></i> <span>{{ __('site.users') }}</span>
              </a>

            </li>

            <li class="treeview">
              <a href="{{ route('categories.index') }}">
                <i class="fa fa-dashboard"></i> <span>{{ __('site.categories') }}</span>
              </a>

            </li>

            <li class="treeview">
              <a href="{{ route('products.index') }}">
                <i class="fa fa-dashboard"></i> <span>{{ __('site.products') }}</span>
              </a>

            </li>

              @endif









          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
