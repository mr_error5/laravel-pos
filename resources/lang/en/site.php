<?php

return [

    'dashboard' => 'Dashboard',
    'users' => 'Users',
    'name' => 'Name',
    'email' => 'Email',
    'action' => 'Action',
    'add' => 'Add',
    'edit' => 'Edit',
    'create' => 'Create',
    'read' => 'Read',
    'update' => 'Update',
    'delete' => 'Delete',
    'search' => 'Search',
    'password' => 'Password',
    'password_confirmation' => 'Password Confirmation',
    'add_success' => 'Add Seccess',
    'updated_successfully' => 'Updated successfully',
       'categories'=>'Categories',
    'image'=>'Image',


    
];
