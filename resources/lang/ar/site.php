<?php

return [

    'dashboard' => 'لوحة التحكم',
    'users' => 'المشرفين ',
    'name' => 'الاسم ',
    'email' => 'البريد الالكتروني ',
    'action' => 'الفعل',
    'add' => 'اضافة',
    'create' => 'اضافة',
    'read' => 'قراءة',
    'update' => 'تعديل',

    'edit' => 'تعديل',
    'delete' => 'حذف',
    'search' => 'بحث',
    'password' => 'كلمة السر',
    'password_confirmation' => 'تاكيد كلمة السر',
    'add_success' => 'تما الاضافه بنجاح',
    'updated_successfully' => 'تم التعديل بنجاح',
    'deleted_successfully' => 'تم الحذف بنجاح',
    'yes' => 'نعم',
    'no' => 'لا',
    'confirm_delete' => 'تاكيد الحذف',
    'categories'=>'الاقسام',
    'image'=>'الصورة',

    'ar' => [
        'name' => 'الاسم بالعربي',
        'description' => 'الوصف بالعربي',
    ],
     'en' => [
        'name' => 'الاسم بالنجليزية',
        'description' => 'الوصف بالانجليزية',
    ],
    // 'en' => [],





];
